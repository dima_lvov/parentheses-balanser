package my.test.task.plugin.jira.customfields;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.AbstractSingleFieldType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.persistence.PersistenceFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;
@Named("myPluginComponent")
@ExportAsService(ParenthesesBalanceCustomField.class)
public class ParenthesesBalanceCustomField extends AbstractSingleFieldType<String> {
    private static final Logger log = LoggerFactory.getLogger(ParenthesesBalanceCustomField.class);

    @Inject
    public ParenthesesBalanceCustomField(CustomFieldValuePersister customFieldValuePersister, GenericConfigManager genericConfigManager) {
    super(customFieldValuePersister, genericConfigManager);
    }

    @Nonnull
    @Override
    protected PersistenceFieldType getDatabaseType() {
        return PersistenceFieldType.TYPE_LIMITED_TEXT;
    }

    @Nullable
    @Override
    protected Object getDbValueFromObject(String s) {
        return s;
    }

    @Nullable
    @Override
    protected String getObjectFromDbValue(@Nonnull Object o) throws FieldValidationException {
        return (String)o;
    }

    public String getStringFromSingularObject(String s) {
        return s;
    }

    public String getSingularObjectFromString(String s) throws FieldValidationException {
        if(s == null) {
            return null;
        }
        throwExceptionIfParenthesesUnbalanced(s);
        return s;
    }

    private void throwExceptionIfParenthesesUnbalanced(final String s) throws FieldValidationException {
        int counter = 0;
        char[] chars = s.toCharArray();
        for(int i = 0; i < chars.length; i++){
            if(chars[i] == '(') counter++;
            if(chars[i] == ')') counter--;
            if(counter < 0) throw new FieldValidationException("Parentheses is un-balanced. Error on position : " + (i + 1));
        }
        if(counter != 0) throw new FieldValidationException("Parentheses is un-balanced. Check quantity of opened parentheses");
    }

    @Override
    public Map<String, Object> getVelocityParameters(final Issue issue,
                                                     final CustomField field,
                                                     final FieldLayoutItem fieldLayoutItem) {
        final Map<String, Object> map = super.getVelocityParameters(issue, field, fieldLayoutItem);
        if (issue == null) {
            return map;
        }
        FieldConfig fieldConfig = field.getRelevantConfig(issue);
        return map;
    }
}