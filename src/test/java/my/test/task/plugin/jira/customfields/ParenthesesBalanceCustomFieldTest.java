package my.test.task.plugin.jira.customfields;

import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.customfields.persistence.PersistenceFieldType;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class ParenthesesBalanceCustomFieldTest extends ParenthesesBalanceCustomField{

    private static ParenthesesBalanceCustomField customField;

    public ParenthesesBalanceCustomFieldTest() {
        super(null, null);
    }

    @BeforeClass
    public static void setUp() {
        customField = new ParenthesesBalanceCustomField(null, null);
    }

    @Test
    public void testGetSingularObjectFromString_WithNull_ShouldReturnNull() throws Exception {
        String  returnedValue = customField.getSingularObjectFromString(null);
        assertThat(returnedValue, is(nullValue()));
    }

    @Test(expected= FieldValidationException.class)
    public void testGetSingularObjectFromString_WithUnbalancedParentheses_ShouldThrowEception_Case_1() throws Exception {
        customField.getSingularObjectFromString("((((())");
    }

    @Test(expected= FieldValidationException.class)
    public void testGetSingularObjectFromString_WithUnbalancedParentheses_ShouldThrowEception_Case_2() throws Exception {
        customField.getSingularObjectFromString("()))(");
    }

    @Test
    public void testGetSingularObjectFromString_WithBalancedParentheses__ShouldReturnTheSameString() throws Exception {
        String[] balancedParenthesesInputs = {"(()()())", "((()))", "(()(()()))", "(a)"};

        for (String balancedParenthesesInput : balancedParenthesesInputs) {
            String  returnedValue = customField.getSingularObjectFromString(balancedParenthesesInput);
            assertThat(returnedValue, equalTo(balancedParenthesesInput));
        }
    }

    @Test
    public void testGetStringFromSingularObject_WithNull_ShouldReturnNull() throws Exception {
        String  returnedValue = customField.getStringFromSingularObject(null);
        assertThat(returnedValue, is(nullValue()));
    }

    @Test
    public void testGetStringFromSingularObject_WithString_ShouldReturnTheSameString() throws Exception {
        String  returnedValue = customField.getStringFromSingularObject("Test_String");
        assertThat(returnedValue, equalTo("Test_String"));
    }

 /********************** Tests of AbstractSingleFieldType overridden protected methods ******************/
    @Test
    public void testGetDatabaseType_ShouldReturn_TYPE_LIMITED_TEXT() {
        assertThat(customField.getDatabaseType(), equalTo(PersistenceFieldType.TYPE_LIMITED_TEXT));
    }

    @Test
    public void testGetDbValueFromObject_WithNull_ShouldReturnNull() {
        Object returnedValue = customField.getDbValueFromObject(null);
        assertThat(returnedValue, is(nullValue()));
    }

    @Test
    public void testGetDbValueFromObject_WithString_ShouldNotModifyInput() {
        Object returnedValue = customField.getDbValueFromObject("Test_object");
        assertThat(returnedValue, instanceOf(String.class));
        assertThat((String)returnedValue, equalTo("Test_object"));
    }

    @Test
    public void testGetObjectFromDbValue_WithNull_ShouldReturnNull() throws FieldValidationException {
        String returnedValue = customField.getObjectFromDbValue(null);
        assertThat(returnedValue, is(nullValue()));
    }

    @Test
    public void testGetObjectFromDbValue_WithString_ShouldNotModifyInput() throws FieldValidationException {
        String returnedValue = customField.getObjectFromDbValue("Other_test_object");
        assertThat(returnedValue, equalTo("Other_test_object"));
    }
}